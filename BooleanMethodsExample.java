public class BooleanMethodsExample {
    public static void main(String[] args) {
              Boolean boolObj = Boolean.valueOf(true);
        boolean boolValue = boolObj.booleanValue();
        System.out.println("Boolean object value: " + boolValue);
       
        boolean bool1 = true;
        boolean bool2 = false;
        int compareResult = Boolean.compare(bool1, bool2);
        System.out.println("Comparison result: " + compareResult);
       
        Boolean boolObj1 = Boolean.valueOf(true);
        Boolean boolObj2 = Boolean.valueOf(false);
        int compareToResult = boolObj1.compareTo(boolObj2);
        System.out.println("Comparison result: " + compareToResult);
       
        Boolean boolObj3 = Boolean.valueOf(true);
        Boolean boolObj4 = Boolean.valueOf(true);
        boolean equalsResult = boolObj3.equals(boolObj4);
        System.out.println("Equals result: " + equalsResult);
    }
}

